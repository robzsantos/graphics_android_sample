package com.example.recyclerviewcase

import android.graphics.Color
import androidx.constraintlayout.widget.ConstraintLayout
import com.github.mikephil.charting.charts.BarChart
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.components.YAxis
import com.github.mikephil.charting.data.*
import com.github.mikephil.charting.formatter.ValueFormatter
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet
import kotlinx.android.synthetic.main.item_line.view.*
import kotlinx.android.synthetic.main.item_vertical_bar.view.*
import java.util.*
import kotlin.math.abs

class LinearModel : BinderEpoxyModel<ConstraintLayout>() {

    private var chart: LineChart? = null

    override fun getDefaultLayout() = R.layout.item_line

    override fun bind(view: ConstraintLayout) {
        super.bind(view)

        chart = view.lineChart as LineChart

        setupChart()

        setData(count = 5, range = 20.0f)

        setupLegend()
    }

    private fun setupChart() {
        chart?.setBackgroundColor(Color.WHITE)
        // disable description text
        chart?.description?.isEnabled = true
        chart?.description?.text = "Título do Gráfico"
        // enable touch gestures
        chart?.setTouchEnabled(true)
        // set listeners
//        chart?.setOnChartValueSelectedListener(this)//depende de implementar esse OnChartValueSelectedListener
        chart?.setDrawGridBackground(false)
/*        // create marker to display box when values are selected
        MyMarkerView mv = new MyMarkerView(this, R.layout.custom_marker_view)
        mv.setChartView(chart?)
        chart?.setMarker(mv)*/

        // enable scaling and dragging
        chart?.isDragEnabled = true
        chart?.setScaleEnabled(true)
        chart?.setDrawGridBackground(false)
        // chart.setScaleXEnabled(true);
        // chart.setScaleYEnabled(true);

        // force pinch zoom along both axis
        chart?.setPinchZoom(true)
        // disable dual axis (only use LEFT axis)
        chart?.axisRight?.isEnabled = false

        // // X-Axis Style // //
        val xAxis: XAxis? = chart?.xAxis
        xAxis?.setDrawGridLines(false)
        xAxis?.position = XAxis.XAxisPosition.BOTTOM
        xAxis?.axisMinimum = 1f //primeiro dia
        xAxis?.axisMaximum = 50f //ultimo dia
        xAxis?.labelCount = 3 //número de labels exibidas com base na média da mínima-máxima
        chart?.let{
            val xAxisFormatter: ValueFormatter =
                DayAxisValueFormatter(it)

            xAxis?.valueFormatter = xAxisFormatter
        }


        // // Y-Axis Style // //
        val yAxis: YAxis? = chart?.axisLeft
        // axis range
        yAxis?.axisMaximum = 40f
        yAxis?.axisMinimum = -40f
    }

    private fun setupLegend() {

        val l: Legend? = chart?.legend

        // draw legend entries as lines
        l?.form = Legend.LegendForm.CIRCLE
        l?.formSize = 16f
        l?.textSize = 16f
        l?.xEntrySpace = 100f
        l?.yEntrySpace = 100f
        l?.formToTextSpace = 6f
        l?.horizontalAlignment = Legend.LegendHorizontalAlignment.CENTER
    }

    private fun setData(count: Int, range: Float) {
        val valuesSet1: ArrayList<Entry> = ArrayList()
        val valuesSet2: ArrayList<Entry> = ArrayList()

        for (i in 0 until count) {
            val value = (Math.random() * range).toFloat() - 10
            valuesSet1.add(Entry(i.toFloat(), value))
        }
        for (i in 0 until count) {
            val value = (Math.random() * range).toFloat() - 10
            valuesSet2.add(Entry(i.toFloat(), value))
        }

        val set1: LineDataSet
        val set2: LineDataSet

        if (chart?.data != null &&
            chart?.data?.dataSetCount?:0 > 0
        ) {
            set1 = chart?.data?.getDataSetByIndex(0) as LineDataSet
            set1.values = valuesSet1
            set1.notifyDataSetChanged()

            set2 = chart?.data?.getDataSetByIndex(1) as LineDataSet
            set2.values = valuesSet2
            set2.notifyDataSetChanged()

            chart?.data?.notifyDataChanged()
            chart?.notifyDataSetChanged()
        } else {
            // create a dataset and give it a type
            set1 = LineDataSet(valuesSet1, "Bebidas")
            set2 = LineDataSet(valuesSet2, "Comidas")
            set1.setDrawIcons(false)
            set2.setDrawIcons(false)

            // black lines and points
            set1.color = Color.GRAY
            set1.setCircleColor(Color.CYAN)

            set2.color = Color.BLUE
            set2.setCircleColor(Color.MAGENTA)

            // line thickness and point size
            set1.lineWidth = 1f
            set1.circleRadius = 3f
            set2.lineWidth = 1f
            set2.circleRadius = 3f

            // draw points as solid circles
            set1.setDrawCircleHole(true)
            set2.setDrawCircleHole(true)

            // customize legend entry
/*            set1.formLineWidth = 1f
            set1.formLineDashEffect = DashPathEffect(floatArrayOf(10f, 5f), 0f)
            set1.formSize = 15f*/

            // text size of values
            set1.valueTextSize = 0f
            set2.valueTextSize = 0f

            // draw selection line as dashed
            set1.enableDashedHighlightLine(10f, 5f, 0f)
            set2.enableDashedHighlightLine(10f, 5f, 0f)

            // set the filled area
//            set1.setDrawFilled(true)
//            set1.fillFormatter = IFillFormatter { dataSet, dataProvider -> lineChart.axisLeft.axisMinimum }

            // set color of filled area
/*          // drawables only supported on api level 18 and above
                val drawable = ContextCompat.getDrawable(this, R.drawable.fade_red)
                set1.fillDrawable = drawable
            */
            val dataSets: ArrayList<ILineDataSet> = ArrayList()
            dataSets.add(set1) // add the data sets
            dataSets.add(set2) // add the data sets

            // create a data object with the data sets
            val data = LineData(dataSets)

            // set data
            chart?.data = data
        }
    }

}