package com.example.recyclerviewcase

data class Salesman(val name:String, val visitas: Int, val visitados: Int, val meta: Int, val positivacao: Int)