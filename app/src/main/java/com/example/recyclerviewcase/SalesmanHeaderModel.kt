package com.example.recyclerviewcase

import android.widget.EditText
import androidx.constraintlayout.widget.ConstraintLayout
import kotlinx.android.synthetic.main.item_salesman_header.view.*

class SalesmanHeaderModel : BinderEpoxyModel<ConstraintLayout>() {

    private var editText: EditText? = null

    override fun getDefaultLayout() = R.layout.item_salesman_header

    override fun bind(view: ConstraintLayout) {
        super.bind(view)

        editText = view.editText as EditText
    }
}