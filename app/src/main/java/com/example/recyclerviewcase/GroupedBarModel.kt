package com.example.recyclerviewcase

import android.graphics.Color
import androidx.constraintlayout.widget.ConstraintLayout
import com.github.mikephil.charting.charts.BarChart
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.components.YAxis
import com.github.mikephil.charting.data.BarData
import com.github.mikephil.charting.data.BarDataSet
import com.github.mikephil.charting.data.BarEntry
import com.github.mikephil.charting.formatter.ValueFormatter
import kotlinx.android.synthetic.main.item_vertical_bar.view.*
import java.util.*
import kotlin.math.abs

class GroupedBarModel : BinderEpoxyModel<ConstraintLayout>() {

    private var chart: BarChart? = null

    override fun getDefaultLayout() = R.layout.item_vertical_bar

    override fun bind(view: ConstraintLayout) {
        super.bind(view)

        chart = view.barChart as BarChart

        setupChart()

        setData(count = 20, range = 40.0f)

        chart?.setVisibleXRangeMaximum(4f)

        setupLegend()
    }

    private fun setupChart() {
        // // Chart Style // //
        chart?.setPinchZoom(false)

        chart?.isScaleXEnabled = false

        // disable dual axis (only use LEFT axis)
        chart?.axisRight?.isEnabled = false

        chart?.setDrawBarShadow(false)

        chart?.setDrawGridBackground(false)

        chart?.description?.isEnabled = false

        chart?.setXAxisRenderer(
            CustomXAxisRenderer(
                chart?.viewPortHandler,
                chart?.xAxis,
                chart?.getTransformer(YAxis.AxisDependency.LEFT)
            )
        )

        chart?.extraBottomOffset = 50f//10f cada linha

        val xAxis: XAxis? = chart?.xAxis
        xAxis?.setDrawGridLines(false)
        xAxis?.position = XAxis.XAxisPosition.BOTTOM
        xAxis?.granularity = 1f // precisa para o zoom, senão começa a duplicar as labels
        xAxis?.valueFormatter = object : ValueFormatter() {

            val mText = arrayOf(
                "Tiago\nNelson\nda Costa",
                "Bernardo\nIago\nMartins",
                "Flávia\nEmanuelly\nda Cunha",
                "Raul\nManoel\nEduardo\nLima",
                "Alícia\nVera\nMilena\nPereira",
                "Marcos\nVinicius\nJuan\nda Conceição",
                "Emanuel\nCarlos\nEduardo\nHenry\nOliveira",
                "Jorge\nJoaquim\nAntonio\nDuarte",
                "Malu\nVanessa\nFerreira",
                "Fabiana\nda Luz",
                "Allana\nRita\nValentina\nMelo",
                "Nicolas\nGalvão",
                "Tânia\nJennifer\nda Costa",
                "Henrique\nThiago\nMário\nDias",
                "Natália\nMariana\nda Mata",
                "Ryan\nAugusto\nGonçalves",
                "Francisca\nTeresinha\nda Conceição",
                "Kamilly\nIsabelle\ndas Neves",
                "Marcela\nLuzia\nBárbara\nCarvalho",
                "Jorge\nBonifácio\nBarillari"
            )

            override fun getFormattedValue(value: Float): String {
                return if (value >= 0 && value < mText.size)
                    mText[value.toInt()]
                else ""
            }
        }

        // // Y-Axis Style // //
        val yAxis: YAxis? = chart?.axisLeft
        // axis range
        yAxis?.axisMaximum = 80f
        yAxis?.axisMinimum = 0f
    }

    private fun setupLegend() {
        // get the legend (only possible after setting data)
        val l: Legend? = chart?.legend

        // draw legend entries as lines
        l?.form = Legend.LegendForm.CIRCLE
        l?.direction = Legend.LegendDirection.LEFT_TO_RIGHT
        l?.formSize = 16f
        l?.textSize = 16f
        l?.xEntrySpace = 10f
        l?.yOffset = 10f
        l?.formToTextSpace = 6f
        l?.isWordWrapEnabled = true
        l?.maxSizePercent = 0.8f
        l?.horizontalAlignment = Legend.LegendHorizontalAlignment.CENTER
    }

    private fun setData(count: Int, range: Float) {

        val valuesSet1: ArrayList<BarEntry> = arrayListOf()
        val valuesSet2: ArrayList<BarEntry> = arrayListOf()
        val valuesSet3: ArrayList<BarEntry> = arrayListOf()

        for (i in 0 until count) {
            valuesSet1.add(sortNewValue(i, count, range))
            valuesSet2.add(sortNewValue(i, count, range))
            valuesSet3.add(sortNewValue(i, count, range))
        }

        val set1: BarDataSet
        val set2: BarDataSet
        val set3: BarDataSet

        if (chart?.data != null &&
            chart?.data?.dataSetCount ?: 0 > 0
        ) {
            set1 = chart?.data?.getDataSetByIndex(0) as BarDataSet
            set2 = chart?.data?.getDataSetByIndex(1) as BarDataSet
            set3 = chart?.data?.getDataSetByIndex(2) as BarDataSet
            set1.values = valuesSet1
            set2.values = valuesSet2
            set3.values = valuesSet3

            chart?.data?.notifyDataChanged()
            chart?.notifyDataSetChanged()
        } else {
            // create a dataset and give it a type
            set1 = BarDataSet(valuesSet1, "Meta")
            set2 = BarDataSet(valuesSet2, "Periodo")
            set3 = BarDataSet(valuesSet3, "Faturado")
//            set1.setDrawIcons(false)

            // black lines and points
            set1.color = Color.BLUE
            set2.color = Color.RED
            set3.color = Color.YELLOW

            // text size of values
            set1.valueTextSize = 0f
            set2.valueTextSize = 0f
            set3.valueTextSize = 0f

            // create a data object with the data sets
            val data = BarData(set1, set2, set3)
//            data.barWidth = 0.9f

            // set data
            chart?.data = data

            val groupSpace = 0.19f
            val barSpace = 0.01f // x2 DataSet
            val barWidth = 0.26f // x2 DataSet
            // (0.26 + 0.01) * 3 + 0.19 = 1.00 -> interval per "group" - para 3 datasets
            // (0.43 + 0.03) * 4 + 0.08 = 1.00 -> interval per "group" - para 4 datasets
            // (0.2 + 0.03) * 2 + 0.08 = 1.00 -> interval per "group" - para 2 datasets

            // specify the width each bar should have
            chart?.barData?.barWidth = barWidth

            chart?.xAxis?.axisMinimum = 0f
            chart?.xAxis?.axisMaximum = 20f
            chart?.xAxis?.setCenterAxisLabels(true) // centraliza a label, quando tem grupo de barras - precisa da validação no formatter
            chart?.groupBars(0f, groupSpace, barSpace)
        }
    }

    private fun sortNewValue(index: Int, count: Int, range: Float): BarEntry {
        val value = abs((Math.random() * range).toFloat() - 10)
        return BarEntry(index.toFloat(), value)
    }


}