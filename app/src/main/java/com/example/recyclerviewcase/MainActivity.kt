package com.example.recyclerviewcase

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*
import kotlin.random.Random

class MainActivity : AppCompatActivity() {

    private val names = arrayOf("Andréia da Silva", "Ricardo Oliveira", "Aline de Souza", "Renato Gaucho", "Kaká",
        "Ronaldo Fenomeno", "Cristiano Ronaldo", "Lionel Messi")

    private var adapter: GraphListAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)

        setupRecyclerView()

        loadGraphics()
    }

    private fun loadGraphics() {
        val salesmanList1 = arrayListOf<Salesman>()
        val salesmanList2 = arrayListOf<Salesman>()
        val salesmanList3 = arrayListOf<Salesman>()

        salesmanList1.add(Salesman(names[Random.nextInt(0, 7)], Random.nextInt(0, 100), Random.nextInt(0, 100), Random.nextInt(0, 100), Random.nextInt(0, 100)))
        salesmanList1.add(Salesman(names[Random.nextInt(0, 7)], Random.nextInt(0, 100), Random.nextInt(0, 100), Random.nextInt(0, 100), Random.nextInt(0, 100)))

        salesmanList2.add(Salesman(names[Random.nextInt(0, 7)], Random.nextInt(0, 100), Random.nextInt(0, 100), Random.nextInt(0, 100), Random.nextInt(0, 100)))
        salesmanList2.add(Salesman(names[Random.nextInt(0, 7)], Random.nextInt(0, 100), Random.nextInt(0, 100), Random.nextInt(0, 100), Random.nextInt(0, 100)))
        salesmanList2.add(Salesman(names[Random.nextInt(0, 7)], Random.nextInt(0, 100), Random.nextInt(0, 100), Random.nextInt(0, 100), Random.nextInt(0, 100)))

        salesmanList3.add(Salesman(names[Random.nextInt(0, 7)], Random.nextInt(0, 100), Random.nextInt(0, 100), Random.nextInt(0, 100), Random.nextInt(0, 100)))

        adapter?.addBarGraphs(arrayListOf(1))
        adapter?.addGraphs(arrayListOf(1))
        adapter?.addGraphs(arrayListOf(1),DataXAxis(40.0f,30.0f,30.0f))
        adapter?.addSalesmanHeader(arrayListOf(1))
        adapter?.addSalesman(arrayListOf(1,2), salesmanList1)
        adapter?.addLinearGraphs(arrayListOf(1))
        adapter?.addBarGraphs(arrayListOf(1,2))
        adapter?.addLinearGraphs(arrayListOf(1))
        adapter?.addGraphs(arrayListOf(1))
        adapter?.addSalesmanHeader(arrayListOf(1))
        adapter?.addSalesman(arrayListOf(1,2,3), salesmanList2)
        adapter?.addGraphs(arrayListOf(1))
        adapter?.addLinearGraphs(arrayListOf(1))
        adapter?.addSalesmanHeader(arrayListOf(1))
        adapter?.addSalesman(arrayListOf(1), salesmanList3)
    }

    private fun setupRecyclerView() {

        recyclerview_graphs.layoutManager = LinearLayoutManager(this)

        adapter = GraphListAdapter()

        recyclerview_graphs.adapter = adapter
    }

}
