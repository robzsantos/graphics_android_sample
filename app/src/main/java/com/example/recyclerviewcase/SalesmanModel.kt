package com.example.recyclerviewcase

import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import kotlinx.android.synthetic.main.item_salesman.view.*
import kotlin.random.Random

class SalesmanModel(val data: Salesman) : BinderEpoxyModel<ConstraintLayout>() {

    private var name: TextView? = null
    private var visitas: TextView? = null
    private var visitados: TextView? = null
    private var meta: TextView? = null
    private var positivacao: TextView? = null

    override fun getDefaultLayout() = R.layout.item_salesman

    override fun bind(view: ConstraintLayout) {
        super.bind(view)

        name = view.name as TextView
        visitas = view.visitas as TextView
        visitados = view.visitados as TextView
        meta = view.meta as TextView
        positivacao = view.positivacao as TextView

        setData(
            data.name,
            data.visitas,
            data.visitados,
            data.meta,
            data.positivacao
        )
    }

    private fun setData(name: String, visitas: Int, visitados: Int, meta: Int, positivacao: Int) {
        this.name?.text = name
        this.visitas?.text = visitas.toString()
        this.visitados?.text = visitados.toString()
        this.meta?.text = "$meta%"
        this.positivacao?.text = "$positivacao%"
    }
}