package com.example.recyclerviewcase

/**
 * Created by Robson on 2019-07-12.
 */
class GraphListAdapter : BaseEpoxyAdapter() {

    fun addGraphs(graphList: ArrayList<Int>,data:DataXAxis? = null) {

        addModels(
            graphList.map { DoughnutModel(data) }.toList()
        )
    }

    fun addBarGraphs(graphList: ArrayList<Int>) {

        addModels(
            graphList.map { GroupedBarModel() }.toList()
        )
    }

    fun addLinearGraphs(graphList: ArrayList<Int>) {

        addModels(
            graphList.map { LinearModel() }.toList()
        )
    }

    fun addSalesmanHeader(salesmanHeaderList: ArrayList<Int>) {

        addModels(
            salesmanHeaderList.map { SalesmanHeaderModel() }.toList()
        )
    }

    fun addSalesman(salesmanList: ArrayList<Int>, dataList: ArrayList<Salesman>) {
        addModels(
            salesmanList.map { SalesmanModel(dataList[it - 1]) }.toList()
        )
    }
}