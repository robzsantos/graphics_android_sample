package com.example.recyclerviewcase

import android.graphics.Color
import androidx.constraintlayout.widget.ConstraintLayout
import com.github.mikephil.charting.charts.PieChart
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.data.PieData
import com.github.mikephil.charting.data.PieDataSet
import com.github.mikephil.charting.data.PieEntry
import com.github.mikephil.charting.formatter.PercentFormatter
import kotlinx.android.synthetic.main.item_doughnut.view.*
import java.util.*

class DoughnutModel(val data:DataXAxis?) : BinderEpoxyModel<ConstraintLayout>() {

    private var chart: PieChart? = null

    override fun getDefaultLayout() = R.layout.item_doughnut

    override fun bind(view: ConstraintLayout) {
        super.bind(view)

        chart = view.pieChart as PieChart

        setupChart()

        setData(count = 3, range = 20.0f)

        setupLegend()
    }

    private fun setupChart() {
        // // Chart Style // //
        chart?.setUsePercentValues(true)
        chart?.description?.isEnabled = false
        chart?.setExtraOffsets(5f, 10f, 5f, 5f)
        chart?.setDrawEntryLabels(false)

//        chart.centerText = generateCenterSpannableText()//Texto do centro

        chart?.isDrawHoleEnabled = true
        chart?.setHoleColor(Color.WHITE)

        chart?.holeRadius = 58f
        chart?.transparentCircleRadius = 61f

        chart?.rotationAngle = 0f
        // enable rotation of the chart by touch
        // enable rotation of the chart by touch
        chart?.isRotationEnabled = false
        chart?.isHighlightPerTapEnabled = true
    }

    private fun setupLegend() {
        // get the legend (only possible after setting data)
        val l: Legend? = chart?.legend

        // draw legend entries as lines
        l?.form = Legend.LegendForm.CIRCLE
        l?.direction = Legend.LegendDirection.LEFT_TO_RIGHT
        l?.formSize = 16f
        l?.textSize = 16f
        l?.xEntrySpace = 10f
        l?.yOffset = 10f
        l?.formToTextSpace = 6f
        l?.isWordWrapEnabled = true
        l?.maxSizePercent = 0.8f
        l?.horizontalAlignment = Legend.LegendHorizontalAlignment.CENTER
    }

    private fun setData(count: Int, range: Float) {

        val entries = ArrayList<PieEntry>()

        val parties = arrayOf("Meta", "Periodo", "Faturado")

        if(data != null){
            entries.addAll(arrayOf(PieEntry(40.0f,parties[0]),
                PieEntry(30.0f,parties[1]),
                PieEntry(30.0f,parties[2]))
            )
        }else{
            for (i in 0 until count) {
                entries.add(
                    PieEntry(
                        (Math.random() * range + range / 5).toFloat(),
                        parties[i % parties.size]
                    )
                )
            }
        }

        val dataSet = PieDataSet(entries, "")

        dataSet.setDrawIcons(false)
        dataSet.valueTextColor = Color.BLACK

        dataSet.sliceSpace = 3f
        dataSet.selectionShift = 5f

        dataSet.colors = listOf<Int>(Color.BLUE, Color.RED, Color.YELLOW)

        val data = PieData(dataSet)
        data.setValueFormatter(PercentFormatter(chart))
        data.setValueTextSize(11f)
        data.setValueTextColor(Color.BLACK)
        chart?.data = data
    }
}